package me.aurora.util.exception;
import me.aurora.domain.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author 郑杰
 * @date 2018/09/18 11:28:04
 * 自定义异常处理
 */
@ControllerAdvice
public class ControllerExceptionHandler {

    @ExceptionHandler(AuroraException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity handleUserNotExistsException(AuroraException e) {
        return ResponseEntity.error(e.getId(),e.getMessage());
    }
}