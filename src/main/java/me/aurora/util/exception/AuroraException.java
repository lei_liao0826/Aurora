package me.aurora.util.exception;

import lombok.Getter;
import java.io.Serializable;

/**
 * @author 郑杰
 * @date 2018/09/18 11:27:40
 * 通用异常处理类
 */
@Getter
public class AuroraException extends RuntimeException implements Serializable {

    private int id;

    public AuroraException(int id,String msg){
        super(msg);
        this.id = id;
    }
}
