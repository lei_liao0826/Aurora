package me.aurora.service.impl;

import me.aurora.domain.Menu;
import me.aurora.domain.Role;
import me.aurora.domain.vo.MenuVo;
import me.aurora.repository.MenuRepo;
import me.aurora.repository.spec.MenuSpec;
import me.aurora.service.MenuService;
import me.aurora.service.dto.MenuDTO;
import me.aurora.service.dto.RoleDTO;
import me.aurora.service.mapper.MenuMapper;
import me.aurora.util.ListSortUtil;
import me.aurora.util.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author 郑杰
 * @date 2018/08/23 17:27:57
 */
@Service
public class MenuServiceImpl implements MenuService {

    @Autowired
    private MenuRepo menuRepo;

    @Autowired
    private MenuMapper menuMapper;

    @Override
    public List<MenuVo> buildMenuUrl(List<MenuDTO> menuList) {
        List<MenuVo> menuVoList = new ArrayList<>();
        for (MenuDTO menu: menuList) {
            if(menu.getLevel()!=1){
                MenuVo menuVo = new MenuVo();
                menuVo.setPath(menu.getUrl());
                menuVo.setComponent(menu.getUrl());
                menuVo.setName(menu.getName());
                menuVoList.add(menuVo);
            }
        }
        return menuVoList;
    }

    @Override
    public List<MenuDTO> findMenusByUserRols(Set<Role> roles) {
        Set<Menu> menuSet = new HashSet<>(menuRepo.findByRoles(roles));
        List<Menu> menus = new ArrayList<>(menuSet);
        List<MenuDTO> menuDTOS = menuMapper.toDto(menus);
        ListSortUtil<MenuDTO> sortList = new ListSortUtil<MenuDTO>();
        sortList.sort(menuDTOS, "soft", "asc");
        return menuDTOS;
    }

    @Override
    public Map getMenuInfo(MenuSpec menuSpec, Pageable pageable) {
        Page<Menu> menuPage = menuRepo.findAll(menuSpec,pageable);
        Page<MenuDTO> menuDTOS = menuPage.map(menuMapper::toDto);
        return PageUtil.buildPage(menuDTOS.getContent(),menuPage.getTotalElements());
    }
}
