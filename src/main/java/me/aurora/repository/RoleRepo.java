package me.aurora.repository;

import me.aurora.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 郑杰
 * @date 2018/08/22
 */
@Repository
public interface RoleRepo extends JpaRepository<Role,Long>,JpaSpecificationExecutor {

    /**
     * 根据name查询role
     * @param name
     * @return
     */
    Role findByName(String name);
}
