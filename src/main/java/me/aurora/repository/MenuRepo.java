package me.aurora.repository;

import me.aurora.domain.Menu;
import me.aurora.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * @author 郑杰
 * @date 2018/08/22
 */
@Repository
public interface MenuRepo extends JpaRepository<Menu,Long>,JpaSpecificationExecutor {

    /**
     * 根据role查询菜单
     * @param roles
     * @return
     */
    List<Menu> findByRoles(Set<Role> roles);
}
