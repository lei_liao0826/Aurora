/*
 Navicat MySQL Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 50559
 Source Host           : localhost:3306
 Source Schema         : aurora

 Target Server Type    : MySQL
 Target Server Version : 50559
 File Encoding         : 65001

 Date: 13/09/2018 21:03:34
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for zj_menu
-- ----------------------------
DROP TABLE IF EXISTS `zj_menu`;
CREATE TABLE `zj_menu`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ico` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pid` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `soft` bigint(20) NOT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `createTime` datetime NULL DEFAULT NULL,
  `updateTime` datetime NULL DEFAULT NULL,
  `level_number` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_cbhg0bi3f1emxkhqqtvca9btx`(`soft`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of zj_menu
-- ----------------------------
INSERT INTO `zj_menu` VALUES (1, NULL, 0, 1, '异常页', 20, '', '2018-08-29 11:40:17', '2018-09-04 07:40:25', 3);
INSERT INTO `zj_menu` VALUES (2, '&#xe69c;', 1, 2, '403', 21, '/exception/403', '2018-08-29 11:40:17', '2018-09-04 07:40:25', 0);
INSERT INTO `zj_menu` VALUES (3, '&#xe61c;', 1, 2, '404', 22, '/exception/404', '2018-08-29 11:40:17', '2018-09-04 07:40:25', 0);
INSERT INTO `zj_menu` VALUES (4, '&#xe64d;', 1, 2, '500', 23, '/exception/500', '2018-08-29 11:40:17', '2018-09-04 07:40:25', 0);
INSERT INTO `zj_menu` VALUES (5, NULL, 0, 1, '用户管理', 1, NULL, '2018-08-29 11:40:17', '2018-09-04 07:40:25', 1);
INSERT INTO `zj_menu` VALUES (6, '&#xe64c;', 5, 2, '用户信息', 2, '/user/index', '2018-08-29 11:40:17', '2018-09-04 07:40:25', 0);
INSERT INTO `zj_menu` VALUES (7, NULL, 0, 1, '系统管理', 10, NULL, '2018-08-29 11:40:17', '2018-09-04 07:40:25', 4);
INSERT INTO `zj_menu` VALUES (8, '&#xe64c;', 7, 2, '菜单管理', 16, '/menu/index', '2018-08-29 11:40:17', '2018-09-04 07:40:25', 0);
INSERT INTO `zj_menu` VALUES (9, '&#xe64c;', 7, 2, '日志管理', 17, '/sysLog/index', '2018-08-29 11:40:17', '2018-09-04 07:40:25', 0);
INSERT INTO `zj_menu` VALUES (10, '&#xe64c;', 7, 2, '权限管理', 12, '/permission/index', '2018-08-29 11:40:17', '2018-09-04 07:40:25', 0);
INSERT INTO `zj_menu` VALUES (11, '&#xe64c;', 7, 2, '角色管理', 11, '/role/index', '2018-08-29 11:40:17', '2018-09-04 07:40:25', 0);

-- ----------------------------
-- Table structure for zj_menus_roles
-- ----------------------------
DROP TABLE IF EXISTS `zj_menus_roles`;
CREATE TABLE `zj_menus_roles`  (
  `menu_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`menu_id`, `role_id`) USING BTREE,
  INDEX `role_id`(`role_id`) USING BTREE,
  CONSTRAINT `zj_menus_roles_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `zj_menu` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `zj_menus_roles_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `zj_role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of zj_menus_roles
-- ----------------------------
INSERT INTO `zj_menus_roles` VALUES (1, 1);
INSERT INTO `zj_menus_roles` VALUES (2, 1);
INSERT INTO `zj_menus_roles` VALUES (3, 1);
INSERT INTO `zj_menus_roles` VALUES (4, 1);
INSERT INTO `zj_menus_roles` VALUES (5, 1);
INSERT INTO `zj_menus_roles` VALUES (6, 1);
INSERT INTO `zj_menus_roles` VALUES (7, 1);
INSERT INTO `zj_menus_roles` VALUES (8, 1);
INSERT INTO `zj_menus_roles` VALUES (9, 1);
INSERT INTO `zj_menus_roles` VALUES (10, 1);
INSERT INTO `zj_menus_roles` VALUES (11, 1);
INSERT INTO `zj_menus_roles` VALUES (1, 2);
INSERT INTO `zj_menus_roles` VALUES (2, 2);
INSERT INTO `zj_menus_roles` VALUES (3, 2);
INSERT INTO `zj_menus_roles` VALUES (4, 2);
INSERT INTO `zj_menus_roles` VALUES (5, 2);
INSERT INTO `zj_menus_roles` VALUES (6, 2);
INSERT INTO `zj_menus_roles` VALUES (7, 2);
INSERT INTO `zj_menus_roles` VALUES (8, 2);
INSERT INTO `zj_menus_roles` VALUES (9, 2);
INSERT INTO `zj_menus_roles` VALUES (10, 2);
INSERT INTO `zj_menus_roles` VALUES (11, 2);

-- ----------------------------
-- Table structure for zj_permission
-- ----------------------------
DROP TABLE IF EXISTS `zj_permission`;
CREATE TABLE `zj_permission`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `perms` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `createTime` datetime NULL DEFAULT NULL,
  `updateTime` datetime NULL DEFAULT NULL,
  `pid` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_djtxn2vldlgrkfk21d155b48i`(`perms`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of zj_permission
-- ----------------------------
INSERT INTO `zj_permission` VALUES (1, 'admin', '超级管理员', '2018-08-29 15:15:12', '2018-08-31 00:00:00', 0);
INSERT INTO `zj_permission` VALUES (2, 'user:select', '用户查询', '2018-08-29 15:15:12', '2018-08-31 00:00:00', 4);
INSERT INTO `zj_permission` VALUES (3, 'log:select', '日志管理', NULL, '2018-09-02 23:11:37', 0);
INSERT INTO `zj_permission` VALUES (4, 'user:all', '用户管理', NULL, '2018-09-02 23:11:31', 0);
INSERT INTO `zj_permission` VALUES (5, 'user:add', '新增用户', '2018-08-31 15:15:12', '2018-08-31 00:00:00', 4);
INSERT INTO `zj_permission` VALUES (6, 'user:update', '更新用户', '2018-08-31 15:15:12', '2018-08-31 00:00:00', 4);
INSERT INTO `zj_permission` VALUES (7, 'user:lock', '禁用用户', NULL, '2018-09-02 12:36:31', 4);
INSERT INTO `zj_permission` VALUES (8, 'permission:all', '权限管理', NULL, '2018-09-02 23:11:04', 0);
INSERT INTO `zj_permission` VALUES (9, 'permission:select', '权限查询', '2018-08-31 14:03:40', '2018-08-31 14:03:40', 8);
INSERT INTO `zj_permission` VALUES (10, 'permission:add', '权限新增', '2018-08-31 14:03:52', '2018-08-31 14:03:52', 8);
INSERT INTO `zj_permission` VALUES (11, 'permission:update', '权限更新', '2018-08-31 14:04:05', '2018-08-31 14:04:05', 8);
INSERT INTO `zj_permission` VALUES (12, 'permission:delete', '权限删除', '2018-08-31 14:04:18', '2018-08-31 14:04:18', 8);
INSERT INTO `zj_permission` VALUES (13, 'role:all', '角色管理', NULL, '2018-09-02 23:10:58', 0);
INSERT INTO `zj_permission` VALUES (14, 'role:select', '角色查询', '2018-08-31 23:19:34', '2018-08-31 23:19:34', 13);
INSERT INTO `zj_permission` VALUES (15, 'role:add', '角色新增', '2018-08-31 23:20:09', '2018-08-31 23:20:09', 13);
INSERT INTO `zj_permission` VALUES (16, 'role:update', '角色更新', '2018-08-31 23:20:25', '2018-08-31 23:20:25', 13);
INSERT INTO `zj_permission` VALUES (17, 'role:delete', '角色删除', '2018-08-31 23:20:40', '2018-08-31 23:20:40', 13);
INSERT INTO `zj_permission` VALUES (19, 'menu:all', '菜单管理', '2018-09-12 13:11:27', '2018-09-12 13:11:27', 0);
INSERT INTO `zj_permission` VALUES (20, 'menu:add', '新增菜单', '2018-09-12 13:11:58', '2018-09-12 13:11:58', 19);
INSERT INTO `zj_permission` VALUES (21, 'menu:update', '更新菜单', '2018-09-12 13:12:21', '2018-09-12 13:12:21', 19);
INSERT INTO `zj_permission` VALUES (22, 'menu:delete', '删除菜单', '2018-09-12 13:12:43', '2018-09-12 13:12:43', 19);
INSERT INTO `zj_permission` VALUES (23, 'menu:select', '菜单搜索', '2018-09-12 13:15:02', '2018-09-12 13:15:02', 19);

-- ----------------------------
-- Table structure for zj_permissions_roles
-- ----------------------------
DROP TABLE IF EXISTS `zj_permissions_roles`;
CREATE TABLE `zj_permissions_roles`  (
  `role_id` bigint(20) NOT NULL,
  `permission_id` bigint(20) NOT NULL,
  PRIMARY KEY (`role_id`, `permission_id`) USING BTREE,
  INDEX `FKoj69ypls2picwdvsx1n0j0rp6`(`permission_id`) USING BTREE,
  CONSTRAINT `FKcdlfc70vro7wbnajmn50wix7w` FOREIGN KEY (`role_id`) REFERENCES `zj_role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKoj69ypls2picwdvsx1n0j0rp6` FOREIGN KEY (`permission_id`) REFERENCES `zj_permission` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of zj_permissions_roles
-- ----------------------------
INSERT INTO `zj_permissions_roles` VALUES (1, 1);
INSERT INTO `zj_permissions_roles` VALUES (3, 1);
INSERT INTO `zj_permissions_roles` VALUES (2, 2);
INSERT INTO `zj_permissions_roles` VALUES (2, 3);
INSERT INTO `zj_permissions_roles` VALUES (2, 9);
INSERT INTO `zj_permissions_roles` VALUES (2, 14);

-- ----------------------------
-- Table structure for zj_role
-- ----------------------------
DROP TABLE IF EXISTS `zj_role`;
CREATE TABLE `zj_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createDateTime` datetime NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `updateDateTime` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of zj_role
-- ----------------------------
INSERT INTO `zj_role` VALUES (1, '2018-08-23 09:13:54', '超级管理员', NULL, '2018-08-23 09:14:02');
INSERT INTO `zj_role` VALUES (2, '2018-08-27 19:31:03', '普通用户', '', '2018-09-01 08:31:08');
INSERT INTO `zj_role` VALUES (3, '2018-09-03 15:17:31', '测试', '1', '2018-09-03 15:17:31');

-- ----------------------------
-- Table structure for zj_syslog
-- ----------------------------
DROP TABLE IF EXISTS `zj_syslog`;
CREATE TABLE `zj_syslog`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createTime` datetime NULL DEFAULT NULL,
  `ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `method` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `operation` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `params` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `time` int(11) NULL DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 459 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of zj_syslog
-- ----------------------------
INSERT INTO `zj_syslog` VALUES (425, '2018-09-03 15:17:31', '127.0.0.1', 'me.aurora.app.rest.system.RoleController.inster()', '新增角色', '  role: me.aurora.domain.Role@1f9e444e  permissions: 1', 94, 'aurora', '内网IP');
INSERT INTO `zj_syslog` VALUES (426, '2018-09-03 22:29:42', '127.0.0.1', 'me.aurora.app.rest.user.UserSecurityController.login()', '用户登录', '  username: zhengjie@tom.com  rememberMe: true', 244, 'aurora', '内网IP');
INSERT INTO `zj_syslog` VALUES (427, '2018-09-03 22:37:55', '127.0.0.1', 'me.aurora.app.rest.user.UserSecurityController.login()', '用户登录', '  username: zhengjie@tom.com  rememberMe: true', 250, 'aurora', '内网IP');
INSERT INTO `zj_syslog` VALUES (428, '2018-09-04 09:03:15', '127.0.0.1', 'me.aurora.app.rest.user.UserSecurityController.login()', '用户登录', '  username: anyone  rememberMe: true', 295, 'anyone', '内网IP');
INSERT INTO `zj_syslog` VALUES (429, '2018-09-04 09:03:58', '127.0.0.1', 'me.aurora.app.rest.user.UserSecurityController.login()', '用户登录', '  username: zhengjie@tom.com  rememberMe: true', 285, 'aurora', '内网IP');
INSERT INTO `zj_syslog` VALUES (430, '2018-09-04 09:08:55', '127.0.0.1', 'me.aurora.app.rest.user.UserSecurityController.login()', '用户登录', '  username: anyone  rememberMe: true', 92, 'anyone', '内网IP');
INSERT INTO `zj_syslog` VALUES (431, '2018-09-04 09:10:23', '127.0.0.1', 'me.aurora.app.rest.user.UserSecurityController.login()', '用户登录', '  username: anyone  rememberMe: true', 43, 'anyone', '内网IP');
INSERT INTO `zj_syslog` VALUES (432, '2018-09-04 20:31:57', '127.0.0.1', 'me.aurora.app.rest.system.PermissionController.inster()', '新增权限', '  permission: me.aurora.domain.Permission@5f62c304', 166, 'aurora', '内网IP');
INSERT INTO `zj_syslog` VALUES (433, '2018-09-04 20:32:00', '127.0.0.1', 'me.aurora.app.rest.system.PermissionController.delete()', '删除权限', '  id: 18', 54, 'aurora', '内网IP');
INSERT INTO `zj_syslog` VALUES (434, '2018-09-04 22:53:38', '127.0.0.1', 'me.aurora.app.rest.user.UserSecurityController.login()', '用户登录', '  username: zhengjie@tom.com  rememberMe: true', 327, 'aurora', '内网IP');
INSERT INTO `zj_syslog` VALUES (435, '2018-09-05 08:46:32', '127.0.0.1', 'me.aurora.app.rest.system.PermissionController.inster()', '新增权限', '  permission: me.aurora.domain.Permission@6e997bf9', 103, 'aurora', '内网IP');
INSERT INTO `zj_syslog` VALUES (436, '2018-09-05 08:46:42', '127.0.0.1', 'me.aurora.app.rest.system.PermissionController.delete()', '删除权限', '  id: 18', 92, 'aurora', '内网IP');
INSERT INTO `zj_syslog` VALUES (437, '2018-09-05 08:48:57', '127.0.0.1', 'me.aurora.app.rest.system.PermissionController.inster()', '新增权限', '  permission: me.aurora.domain.Permission@3c4a2728', 115, 'aurora', '内网IP');
INSERT INTO `zj_syslog` VALUES (438, '2018-09-05 08:49:20', '127.0.0.1', 'me.aurora.app.rest.system.PermissionController.delete()', '删除权限', '  id: 19', 97, 'aurora', '内网IP');
INSERT INTO `zj_syslog` VALUES (439, '2018-09-05 08:49:47', '127.0.0.1', 'me.aurora.app.rest.system.PermissionController.inster()', '新增权限', '  permission: me.aurora.domain.Permission@520f1eb2', 36, 'aurora', '内网IP');
INSERT INTO `zj_syslog` VALUES (440, '2018-09-05 08:58:21', '127.0.0.1', 'me.aurora.app.rest.system.PermissionController.delete()', '删除权限', '  id: 20', 89, 'aurora', '内网IP');
INSERT INTO `zj_syslog` VALUES (441, '2018-09-05 09:24:02', '127.0.0.1', 'me.aurora.app.rest.system.PermissionController.delete()', '删除权限', '  id: 23', 61, 'aurora', '内网IP');
INSERT INTO `zj_syslog` VALUES (442, '2018-09-10 10:19:09', '127.0.0.1', 'me.aurora.app.rest.user.UserSecurityController.login()', '用户登录', '  username: anyone  rememberMe: true', 466, 'anyone', '内网IP');
INSERT INTO `zj_syslog` VALUES (443, '2018-09-12 09:26:09', '127.0.0.1', 'me.aurora.app.rest.user.UserSecurityController.login()', '用户登录', '  username: zhengjie@tom.com  rememberMe: true', 439, 'aurora', '内网IP');
INSERT INTO `zj_syslog` VALUES (444, '2018-09-12 11:39:44', '127.0.0.1', 'me.aurora.app.rest.system.PermissionController.inster()', '新增权限', '  permission: me.aurora.domain.Permission@7757ff4e', 152, 'aurora', '内网IP');
INSERT INTO `zj_syslog` VALUES (445, '2018-09-12 11:39:45', '127.0.0.1', 'me.aurora.app.rest.system.PermissionController.inster()', '新增权限', '  permission: me.aurora.domain.Permission@6dbb1b17', 19, 'aurora', '内网IP');
INSERT INTO `zj_syslog` VALUES (446, '2018-09-12 11:42:24', '127.0.0.1', 'me.aurora.app.rest.system.PermissionController.inster()', '新增权限', '  permission: me.aurora.domain.Permission@77f23aeb', 80, 'aurora', '内网IP');
INSERT INTO `zj_syslog` VALUES (447, '2018-09-12 11:42:31', '127.0.0.1', 'me.aurora.app.rest.system.PermissionController.inster()', '新增权限', '  permission: me.aurora.domain.Permission@168b4690', 16, 'aurora', '内网IP');
INSERT INTO `zj_syslog` VALUES (448, '2018-09-12 11:46:16', '127.0.0.1', 'me.aurora.app.rest.system.PermissionController.inster()', '新增权限', '  permission: me.aurora.domain.Permission@62e61be4', 123, 'aurora', '内网IP');
INSERT INTO `zj_syslog` VALUES (449, '2018-09-12 11:46:44', '127.0.0.1', 'me.aurora.app.rest.system.PermissionController.delete()', '删除权限', '  id: 18', 52, 'aurora', '内网IP');
INSERT INTO `zj_syslog` VALUES (450, '2018-09-12 12:50:23', '127.0.0.1', 'me.aurora.app.rest.user.UserSecurityController.login()', '用户登录', '  username: zhengjie@tom.com  rememberMe: true', 216, 'aurora', '内网IP');
INSERT INTO `zj_syslog` VALUES (451, '2018-09-12 13:11:29', '127.0.0.1', 'me.aurora.app.rest.system.PermissionController.inster()', '新增权限', '  permission: me.aurora.domain.Permission@bf024f3', 5, 'aurora', '内网IP');
INSERT INTO `zj_syslog` VALUES (452, '2018-09-12 13:11:30', '127.0.0.1', 'me.aurora.app.rest.system.PermissionController.inster()', '新增权限', '  permission: me.aurora.domain.Permission@5c760c53', 126, 'aurora', '内网IP');
INSERT INTO `zj_syslog` VALUES (453, '2018-09-12 13:11:58', '127.0.0.1', 'me.aurora.app.rest.system.PermissionController.inster()', '新增权限', '  permission: me.aurora.domain.Permission@77a42530', 41, 'aurora', '内网IP');
INSERT INTO `zj_syslog` VALUES (454, '2018-09-12 13:12:21', '127.0.0.1', 'me.aurora.app.rest.system.PermissionController.inster()', '新增权限', '  permission: me.aurora.domain.Permission@6ec358aa', 76, 'aurora', '内网IP');
INSERT INTO `zj_syslog` VALUES (455, '2018-09-12 13:12:43', '127.0.0.1', 'me.aurora.app.rest.system.PermissionController.inster()', '新增权限', '  permission: me.aurora.domain.Permission@3d1d9290', 82, 'aurora', '内网IP');
INSERT INTO `zj_syslog` VALUES (456, '2018-09-12 13:15:02', '127.0.0.1', 'me.aurora.app.rest.system.PermissionController.inster()', '新增权限', '  permission: me.aurora.domain.Permission@5be6d67f', 84, 'aurora', '内网IP');
INSERT INTO `zj_syslog` VALUES (457, '2018-09-13 09:17:06', '127.0.0.1', 'me.aurora.app.rest.user.UserSecurityController.login()', '用户登录', '  username: admin  rememberMe: true', 155, 'Login failed：admin', '内网IP');
INSERT INTO `zj_syslog` VALUES (458, '2018-09-13 09:17:09', '127.0.0.1', 'me.aurora.app.rest.user.UserSecurityController.login()', '用户登录', '  username: zhengjie@tom.com  rememberMe: true', 114, 'aurora', '内网IP');

-- ----------------------------
-- Table structure for zj_user
-- ----------------------------
DROP TABLE IF EXISTS `zj_user`;
CREATE TABLE `zj_user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `createDateTime` datetime NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `enabled` bigint(20) NULL DEFAULT NULL,
  `lastLoginTime` datetime NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_kpubos9gc2cvtkb0thktkbkes`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of zj_user
-- ----------------------------
INSERT INTO `zj_user` VALUES (1, 'https://www.zhengjie.me/images/avatar.jpg', '2018-08-23 09:11:56', 'zhengjie@tom.com', 1, '2018-09-13 09:17:09', '048af245d18d48a2d2d94d3823b8706a', 'aurora');
INSERT INTO `zj_user` VALUES (3, 'https://www.zhengjie.me/images/avatar.jpg', '2018-08-28 11:03:00', 'anyone@qq.com', 1, '2018-09-10 10:19:09', '508691feb86e261821bbbf624de99b3a', 'anyone');
INSERT INTO `zj_user` VALUES (4, 'https://www.zhengjie.me/images/avatar.jpg', '2018-08-28 13:40:25', '862747045@qq.com', 1, '2018-08-28 13:40:54', '2f31dbdfbac6d66fb8bfdbb51c667fea', 'hy');

-- ----------------------------
-- Table structure for zj_users_roles
-- ----------------------------
DROP TABLE IF EXISTS `zj_users_roles`;
CREATE TABLE `zj_users_roles`  (
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE,
  INDEX `role_id`(`role_id`) USING BTREE,
  CONSTRAINT `zj_users_roles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `zj_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `zj_users_roles_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `zj_role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of zj_users_roles
-- ----------------------------
INSERT INTO `zj_users_roles` VALUES (1, 1);
INSERT INTO `zj_users_roles` VALUES (4, 1);
INSERT INTO `zj_users_roles` VALUES (3, 2);

SET FOREIGN_KEY_CHECKS = 1;
