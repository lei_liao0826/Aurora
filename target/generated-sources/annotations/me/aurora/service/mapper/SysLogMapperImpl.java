package me.aurora.service.mapper;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import me.aurora.domain.SysLog;
import me.aurora.service.dto.SysLogDTO;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-09-18T09:49:32+0800",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_171 (Oracle Corporation)"
)
@Component
public class SysLogMapperImpl implements SysLogMapper {

    @Override
    public SysLog toEntity(SysLogDTO dto) {
        if ( dto == null ) {
            return null;
        }

        SysLog sysLog = new SysLog();

        sysLog.setId( dto.getId() );
        sysLog.setUsername( dto.getUsername() );
        sysLog.setOperation( dto.getOperation() );
        sysLog.setTime( dto.getTime() );
        sysLog.setMethod( dto.getMethod() );
        sysLog.setParams( dto.getParams() );
        sysLog.setIp( dto.getIp() );
        sysLog.setLocation( dto.getLocation() );
        sysLog.setCreateTime( dto.getCreateTime() );

        return sysLog;
    }

    @Override
    public SysLogDTO toDto(SysLog entity) {
        if ( entity == null ) {
            return null;
        }

        SysLogDTO sysLogDTO = new SysLogDTO();

        sysLogDTO.setId( entity.getId() );
        sysLogDTO.setUsername( entity.getUsername() );
        sysLogDTO.setOperation( entity.getOperation() );
        sysLogDTO.setTime( entity.getTime() );
        sysLogDTO.setMethod( entity.getMethod() );
        sysLogDTO.setParams( entity.getParams() );
        sysLogDTO.setIp( entity.getIp() );
        sysLogDTO.setLocation( entity.getLocation() );
        sysLogDTO.setCreateTime( entity.getCreateTime() );

        return sysLogDTO;
    }

    @Override
    public List<SysLog> toEntity(List<SysLogDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<SysLog> list = new ArrayList<SysLog>( dtoList.size() );
        for ( SysLogDTO sysLogDTO : dtoList ) {
            list.add( toEntity( sysLogDTO ) );
        }

        return list;
    }

    @Override
    public List<SysLogDTO> toDto(List<SysLog> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<SysLogDTO> list = new ArrayList<SysLogDTO>( entityList.size() );
        for ( SysLog sysLog : entityList ) {
            list.add( toDto( sysLog ) );
        }

        return list;
    }
}
