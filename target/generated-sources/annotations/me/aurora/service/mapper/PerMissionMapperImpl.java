package me.aurora.service.mapper;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import me.aurora.domain.Permission;
import me.aurora.service.dto.PermissionDTO;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-09-18T09:49:32+0800",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_171 (Oracle Corporation)"
)
@Component
public class PerMissionMapperImpl implements PerMissionMapper {

    @Override
    public Permission toEntity(PermissionDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Permission permission = new Permission();

        permission.setId( dto.getId() );
        permission.setPerms( dto.getPerms() );
        permission.setPid( dto.getPid() );
        permission.setRemark( dto.getRemark() );
        permission.setCreateTime( dto.getCreateTime() );
        permission.setUpdateTime( dto.getUpdateTime() );

        return permission;
    }

    @Override
    public PermissionDTO toDto(Permission entity) {
        if ( entity == null ) {
            return null;
        }

        PermissionDTO permissionDTO = new PermissionDTO();

        permissionDTO.setId( entity.getId() );
        permissionDTO.setPerms( entity.getPerms() );
        permissionDTO.setPid( entity.getPid() );
        permissionDTO.setRemark( entity.getRemark() );
        permissionDTO.setCreateTime( entity.getCreateTime() );
        permissionDTO.setUpdateTime( entity.getUpdateTime() );

        return permissionDTO;
    }

    @Override
    public List<Permission> toEntity(List<PermissionDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Permission> list = new ArrayList<Permission>( dtoList.size() );
        for ( PermissionDTO permissionDTO : dtoList ) {
            list.add( toEntity( permissionDTO ) );
        }

        return list;
    }

    @Override
    public List<PermissionDTO> toDto(List<Permission> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<PermissionDTO> list = new ArrayList<PermissionDTO>( entityList.size() );
        for ( Permission permission : entityList ) {
            list.add( toDto( permission ) );
        }

        return list;
    }
}
