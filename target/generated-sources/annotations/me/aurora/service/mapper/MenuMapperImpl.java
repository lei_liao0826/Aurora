package me.aurora.service.mapper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Generated;
import me.aurora.domain.Menu;
import me.aurora.domain.Role;
import me.aurora.service.dto.MenuDTO;
import me.aurora.service.dto.RoleDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-09-18T09:49:32+0800",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_171 (Oracle Corporation)"
)
@Component
public class MenuMapperImpl implements MenuMapper {

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public Menu toEntity(MenuDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Menu menu = new Menu();

        menu.setId( dto.getId() );
        menu.setSoft( dto.getSoft() );
        menu.setIco( dto.getIco() );
        menu.setName( dto.getName() );
        menu.setUrl( dto.getUrl() );
        menu.setPid( dto.getPid() );
        menu.setLevel( dto.getLevel() );
        menu.setLevelNum( dto.getLevelNum() );
        menu.setRoles( roleDTOSetToRoleSet( dto.getRoles() ) );
        menu.setCreateTime( dto.getCreateTime() );
        menu.setUpdateTime( dto.getUpdateTime() );

        return menu;
    }

    @Override
    public MenuDTO toDto(Menu entity) {
        if ( entity == null ) {
            return null;
        }

        MenuDTO menuDTO = new MenuDTO();

        menuDTO.setId( entity.getId() );
        menuDTO.setSoft( entity.getSoft() );
        menuDTO.setIco( entity.getIco() );
        menuDTO.setName( entity.getName() );
        menuDTO.setUrl( entity.getUrl() );
        menuDTO.setPid( entity.getPid() );
        menuDTO.setLevel( entity.getLevel() );
        menuDTO.setLevelNum( entity.getLevelNum() );
        menuDTO.setCreateTime( entity.getCreateTime() );
        menuDTO.setUpdateTime( entity.getUpdateTime() );
        menuDTO.setRoles( roleSetToRoleDTOSet( entity.getRoles() ) );

        return menuDTO;
    }

    @Override
    public List<Menu> toEntity(List<MenuDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Menu> list = new ArrayList<Menu>( dtoList.size() );
        for ( MenuDTO menuDTO : dtoList ) {
            list.add( toEntity( menuDTO ) );
        }

        return list;
    }

    @Override
    public List<MenuDTO> toDto(List<Menu> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<MenuDTO> list = new ArrayList<MenuDTO>( entityList.size() );
        for ( Menu menu : entityList ) {
            list.add( toDto( menu ) );
        }

        return list;
    }

    protected Set<Role> roleDTOSetToRoleSet(Set<RoleDTO> set) {
        if ( set == null ) {
            return null;
        }

        Set<Role> set1 = new HashSet<Role>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( RoleDTO roleDTO : set ) {
            set1.add( roleMapper.toEntity( roleDTO ) );
        }

        return set1;
    }

    protected Set<RoleDTO> roleSetToRoleDTOSet(Set<Role> set) {
        if ( set == null ) {
            return null;
        }

        Set<RoleDTO> set1 = new HashSet<RoleDTO>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( Role role : set ) {
            set1.add( roleMapper.toDto( role ) );
        }

        return set1;
    }
}
