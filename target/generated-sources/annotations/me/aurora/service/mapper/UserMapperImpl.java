package me.aurora.service.mapper;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import me.aurora.domain.User;
import me.aurora.service.dto.UserDTO;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-09-18T11:31:47+0800",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_171 (Oracle Corporation)"
)
@Component
public class UserMapperImpl implements UserMapper {

    @Override
    public User toEntity(UserDTO dto) {
        if ( dto == null ) {
            return null;
        }

        User user = new User();

        user.setId( dto.getId() );
        user.setUsername( dto.getUsername() );
        user.setEmail( dto.getEmail() );
        user.setEnabled( dto.getEnabled() );
        user.setAvatar( dto.getAvatar() );
        user.setCreateDateTime( dto.getCreateDateTime() );
        user.setLastLoginTime( dto.getLastLoginTime() );

        return user;
    }

    @Override
    public UserDTO toDto(User entity) {
        if ( entity == null ) {
            return null;
        }

        UserDTO userDTO = new UserDTO();

        userDTO.setId( entity.getId() );
        userDTO.setUsername( entity.getUsername() );
        userDTO.setEmail( entity.getEmail() );
        userDTO.setEnabled( entity.getEnabled() );
        userDTO.setAvatar( entity.getAvatar() );
        userDTO.setCreateDateTime( entity.getCreateDateTime() );
        userDTO.setLastLoginTime( entity.getLastLoginTime() );

        return userDTO;
    }

    @Override
    public List<User> toEntity(List<UserDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<User> list = new ArrayList<User>( dtoList.size() );
        for ( UserDTO userDTO : dtoList ) {
            list.add( toEntity( userDTO ) );
        }

        return list;
    }

    @Override
    public List<UserDTO> toDto(List<User> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<UserDTO> list = new ArrayList<UserDTO>( entityList.size() );
        for ( User user : entityList ) {
            list.add( toDto( user ) );
        }

        return list;
    }

    @Override
    public UserDTO toDto(User user, String rolesSelect) {
        if ( user == null && rolesSelect == null ) {
            return null;
        }

        UserDTO userDTO = new UserDTO();

        if ( user != null ) {
            userDTO.setId( user.getId() );
            userDTO.setUsername( user.getUsername() );
            userDTO.setEmail( user.getEmail() );
            userDTO.setEnabled( user.getEnabled() );
            userDTO.setAvatar( user.getAvatar() );
            userDTO.setCreateDateTime( user.getCreateDateTime() );
            userDTO.setLastLoginTime( user.getLastLoginTime() );
        }
        if ( rolesSelect != null ) {
            userDTO.setRolesSelect( rolesSelect );
        }

        return userDTO;
    }
}
